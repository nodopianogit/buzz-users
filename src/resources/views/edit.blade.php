@extends('layouts.buzz')

@section('title')
  {{ __('Modifica utente') }}
@endsection

@section('content')

  <users-edit :user-id="{{ $utente->id }}"></users-edit>

@endsection