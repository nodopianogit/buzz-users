@extends('layouts.buzz')

@section('title')
  {{ __('Utenti') }}
@endsection

@section('content')

  <users-index></users-index>

@endsection