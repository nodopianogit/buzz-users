<?php

Route::group(['middleware' => ['web','auth']], function() {
    Route::get('profilo', 'Nodopiano\Buzz\Users\Controllers\PersonalProfileController@index')->name('utenti.user.index');
    Route::post('profilo', 'Nodopiano\Buzz\Users\Controllers\PersonalProfileController@update')->name('utenti.user.update');
});

Route::group(['prefix' => 'admin', 'middleware' => ['web','auth', 'role:admin']], function () {
    Route::get('/utenti', 'Nodopiano\Buzz\Users\Controllers\UsersController@index')->name('utenti.admin.index');
    Route::get('/utenti/crea', 'Nodopiano\Buzz\Users\Controllers\UsersController@create')->name('utenti.admin.create');
    Route::post('/utenti', 'Nodopiano\Buzz\Users\Controllers\UsersController@store')->name('utenti.admin.store');
    Route::get('/utenti/{id}', 'Nodopiano\Buzz\Users\Controllers\UsersController@edit')->name('utenti.admin.edit');
    Route::post('/utenti/{id}', 'Nodopiano\Buzz\Users\Controllers\UsersController@update')->name('utenti.admin.update');
    Route::delete('/utenti/{id}', 'Nodopiano\Buzz\Users\Controllers\UsersController@destroy')->name('utenti.admin.destroy');
});

Route::group(['prefix' => 'api', 'middleware' => ['web','auth']], function () {
    Route::get('/utenti/list', 'Nodopiano\Buzz\Users\Controllers\Api\UsersController@list');
    Route::get('/utenti/{id}', 'Nodopiano\Buzz\Users\Controllers\Api\UsersController@show');
    Route::post('/utenti/{id}', 'Nodopiano\Buzz\Users\Controllers\Api\UsersController@update');
});

Route::group(['prefix' => 'api', 'middleware' => ['web','auth', 'role:admin']], function () {
    Route::get('/utenti', 'Nodopiano\Buzz\Users\Controllers\Api\UsersController@index');
    Route::post('/utenti', 'Nodopiano\Buzz\Users\Controllers\Api\UsersController@store');
    Route::delete('/utenti/{id}', 'Nodopiano\Buzz\Users\Controllers\Api\UsersController@destroy');
});