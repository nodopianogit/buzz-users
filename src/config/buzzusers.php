<?php

return [

	/*--------------------------------
    REPOSITORY - repository to handle class User
        default: 'Nodopiano\Buzz\Users\Repositories\EloquentUserRepository'
---------------------------------*/
	'repository' => 'Nodopiano\Buzz\Users\Repositories\EloquentUserRepository',

	/*--------------------------------
	FORM REQUEST
		default: Nodopiano\Buzz\Users\Requests\UserValidation
---------------------------------*/
	'creation_request' => Nodopiano\Buzz\Users\Requests\UserValidation::class,

	'update_request' => Nodopiano\Buzz\Users\Requests\UserValidation::class,

	'profile_update_request' => Nodopiano\Buzz\Users\Requests\ProfileValidation::class,

	/*--------------------------------
    API RESOURCE - mapping class to decorate API
        default: Nodopiano\Buzz\Users\Resources\User::class
---------------------------------*/
	'api_resource' => Nodopiano\Buzz\Users\Resources\User::class,

	/*--------------------------------
	SEARCH - column to search in database
		default: 'name'
---------------------------------*/
	'search_fields' => ['name'],

	/*--------------------------------
	PAGINATE - number of results per page
		default: 15
---------------------------------*/
	'results_per_page' => 15,
];
