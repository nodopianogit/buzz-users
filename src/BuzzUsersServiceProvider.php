<?php

namespace Nodopiano\Buzz\Users;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;

class BuzzUsersServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/routes.php');
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'buzz-users');

        $this->publishes([
            __DIR__ . '/config/buzzusers.php' => config_path('buzzusers.php'),
            __DIR__ . '/resources/views' => resource_path('views/vendor/buzz-users')
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/config/buzzusers.php',
            'buzzusers'
        );

        $this->app->bind('Nodopiano\Buzz\Users\Repositories\UserRepository', config('buzzusers.repository'));
        //pubblico alias per form request
        $this->app->booting(function () {
            $loader = AliasLoader::getInstance();
            // dd(config('buzzusers.creation_request'));
            $loader->alias('UserCreation', config('buzzusers.creation_request'));
            $loader->alias('UserUpdate', config('buzzusers.update_request'));
            $loader->alias('ProfileUpdate', config('buzzusers.profile_update_request'));
        });
    }
}
