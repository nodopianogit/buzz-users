<?php
namespace Nodopiano\Buzz\Users\Repositories;

use App\User;

class EloquentUserRepository implements UserRepository
{
    protected $model;

    public function __construct(User $user)
    {
        $this->model = $user;
    }

    public function load($id)
    {
        $this->model = $this->model->findOrFail($id);
    }

    public function show($id)
    {
        return $this->model->findOrFail($id);
    }

    public function delete($id)
    {
        return $this->model->findOrFail($id)->delete();
    }

    public function create($attributes = [])
    {
        if (isset($attributes['password'])) {
            $attributes['password'] = bcrypt($attributes['password']);
        }
        return $this->model->create($attributes);
    }

    public function update($id, $attributes = [])
    {
        if (isset($attributes['password']) && !empty($attributes['password'])) {
            $attributes['password'] = bcrypt($attributes['password']);
        }
        return $this->model->findOrFail($id)->update($attributes);
    }

    public function filter($columns = null, $value = null)
    {
        if ($value && $columns) {
            foreach ($columns as $column) {
                $this->model = $this->model->orWhere($column, 'LIKE', '%'.$value.'%');
            }
        }
        return $this;
    }

    public function sort($column = null, $desc = false)
    {
        if ($column) {
            $order = ($desc)? 'desc' : 'asc';
            $this->model = $this->model->orderBy($column, $order);
        }
        return $this;
    }

    public function get()
    {
        return $this->model->get();
    }

    public function paginate($pageSize)
    {
        return $this->model->paginate($pageSize);
    }

    public function list()
    {
        return $this->model->select('id', 'name')->get();
    }
}
