<?php

namespace Nodopiano\Buzz\Users\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required'
            // 'email' => 'required|email|unique:users,email',
            // 'password' => 'required|confirmed'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => "Il nome dell'utente è un campo obbligatorio.",
            'email.required' => "L'email dell'utente è un campo obbligatorio.",
            'email.email' => "Il formato dell'email è incorretto.",
            'email.unique' => "Esiste già un utente con questa email.",
            'password.required' => "Inserire una password.",
            'password.confirmed' => "Le due password inserite non sono uguali"
        ];
    }
}
