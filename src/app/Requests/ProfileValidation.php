<?php

namespace Nodopiano\Buzz\Users\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'confirmed'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => "Il nome dell'utente è un campo obbligatorio.",
            'email.required' => "L'email dell'utente è un campo obbligatorio.",
            'email.email' => "Il formato dell'email è incorretto.",
            'password.confirmed' => "Le due password inserite non sono uguali"
        ];
    }
}
