<?php

namespace Nodopiano\Buzz\Users\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Nodopiano\Buzz\Users\Repositories\UserRepository;

use UserCreation;
use UserUpdate;

use App\User;

class UsersController extends Controller
{
    protected $repo;

    public function __construct(UserRepository $repository)
    {
        $this->repo = $repository;
        $this->api = config('buzzusers.api_resource');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $columns = (config('buzzusers.search_fields') == null)? ['name'] : config('buzzusers.search_fields');

        return $this->api::collection($this->repo->filter($columns, $request->filter)->sort($request->sort, $request->order == 'desc')->paginate(config('buzzusers.results_per_page')));
    }

    public function list()
    {
        return $this->repo->list();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserCreation $request)
    {
        return response()->json($this->repo->create($request->all()));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new $this->api($this->repo->show($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdate $request, $id)
    {
        $user = \Auth::user();
        if ($user == null || (! $user->hasRole('admin') && $user->id != $id)) {
            abort(403);
        }

        return response()->json($this->repo->update(
            $id, $request->all()
        ));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return response()->json($this->repo->delete($id));
    }
}
