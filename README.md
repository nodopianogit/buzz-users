# Buzz-Users
Package pensato per la gestione degli utenti, includendo anche la gestione dei Ruoli. 
E' pensato per funzionare con il model App\User offerto da Laravel e con le migration offerte di base dal framework.

## Configurazione
Tramite il file `config/buzzusers.php` è possibile modificare il comportamento del package per quanto riguarda le funzioni di:
* validazione input utente
* filtro ricerca
* risultati per pagina nelle api

## Customizzazione
Nel caso in cui, per qualsiasi motivo, sia necessario modificare il comportamento del package nella gestione utenti, è sufficiente modificare le funzionalità di un componente: il repository.
### Repository
Il repository si occupa di gestire l'interfaccia dell'applicazione con la tabella utenti nel database.
Nel caso in cui si volesse modificare questa funzionalità, è sufficiente creare una classe che implementi l'interfaccia **Nodopiano\Buzz\Users\Repositories\UserRepository** (oppure estendere il repository già esistente): l'implementazione di tutti i metodi previsti dal contratto garantisce il corretto funzionamento del package.